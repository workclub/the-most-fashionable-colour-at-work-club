#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os

import jinja2
import webapp2

from google.appengine.ext import ndb

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class Colour(ndb.Model):
    """Models an individual Colour entry with name and hex."""
    hex_value = ndb.StringProperty(indexed=False)
    name = ndb.StringProperty(indexed=False)


class MainHandler(webapp2.RequestHandler):
    def get(self):
        current_colour = Colour.query().get()

        new_colour = self.request.get('hex_value')
        if new_colour != "":
            if current_colour is None:
                current_colour = Colour()
            current_colour.hex_value = new_colour
            name = self.request.get('name')
            if name == "":
                # set default name to hex value if none set
                name = new_colour
            current_colour.name = name
            current_colour.put()


        template_values = {
            'current_colour': current_colour
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
